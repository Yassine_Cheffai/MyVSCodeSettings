{
    "workbench.colorCustomizations": { 
        "statusBar.background" : "#1A1A1A", 
        "statusBar.noFolderBackground" : "#212121", 
        "statusBar.debuggingBackground": "#263238"
    },
    "files.autoSave": "onFocusChange",
    "breadcrumbs.enabled": true,
    "window.menuBarVisibility": "default",
    "editor.minimap.enabled": false,
    "editor.quickSuggestions": {
        "other": false,
        "comments": false,
        "strings": false
    },
    "workbench.statusBar.feedback.visible": false,
    "python.pythonPath": "/usr/bin/python3",
    "terminal.integrated.rendererType": "dom",
    "python.linting.pep8Enabled": true,
    "explorer.decorations.badges": false,
    "explorer.decorations.colors": false,
    "editor.snippetSuggestions": "none",
    "editor.suggestOnTriggerCharacters": false,
    "editor.wordBasedSuggestions": false,
    "workbench.editor.enablePreview": false,
    "workbench.editor.enablePreviewFromQuickOpen": false,
    "window.titleBarStyle": "custom",
    "editor.parameterHints.enabled": false
}
